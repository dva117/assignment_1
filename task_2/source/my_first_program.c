#include <stdio.h>
// calculates the sum of two numbers.

int main(int argc, char** argv)
{
    int first_number;
    int second_number;
    int sum;

    printf("please enter first number:");
    scanf("%d", &first_number);
    printf("please enter second number:");
    scanf("%d", &second_number);

    sum = first_number + second_number;

    printf("%d + %d = %d\n", first_number, second_number, sum);

    return 0;
}
