#include <stdio.h>
// calculates the price of a article excluding tax and how much the tax is.

int main(int argc, char** argv)
{
    float price;
    int vat;
    float nontax;

    printf("Price on article (including VAT):");
    scanf("%f", &price);
    printf("VAT (percentage as an integer):");
    scanf("%d", &vat);

    nontax = price/((100+vat*1.00)/100);

    printf("Price excluding VAT is %.2f SEK", nontax);
    printf("\nThe VAT is %.2f SEK\n", price-nontax);

    return 0;
}
